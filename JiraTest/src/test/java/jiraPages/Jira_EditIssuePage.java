package jiraPages;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import commonUtils.TestReport;

/**
 * Jira_EditIssuePage has the page components of Edit Issue dialog
 * opened by clicking on the Edit button. One difference from the 
 * Create Issue Page is the elements are in tabs rather than a single page
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */

public class Jira_EditIssuePage extends LoadableComponent<Jira_EditIssuePage>{

	private WebDriver driver;
	
	private HashMap<String,WebElement> ObjMap;
	private HashMap<WebElement,List<String>> TabMap;
	private List<String> tab1Elem;
	private List<String> tab2Elem;
	private List<String> tab3Elem;
	private List<String> selectElem = Arrays.asList("SecurityLevel","Vendor","AnimalsType","Animals"); 
	private List<String> tagElem = Arrays.asList("Components","AffectVersions","FixVersions","Labels");
	
	@FindBy(id="edit-issue") WebElement editBtn;
	
	@FindBy(xpath="//a[@href='#tab-0']") WebElement tabOne;
	
	@FindBy(xpath="//a[@href='#tab-1']") WebElement tabTwo;
	
	@FindBy(xpath="//a[@href='#tab-2']") WebElement tabThree;
	
	@FindBy(id="edit-issue-dialog") WebElement dialog_EditIssue;
	
	@FindBy(id="project-field") WebElement Project;
	
	@FindBy(id="issuetype-field") WebElement IssueType;
	
	@FindBy(id="summary") WebElement Summary;
	
	@FindBy(id="security") WebElement SecurityLevel;
	
	@FindBy(id="priority-field") WebElement Priority;
	
	@FindBy(id="duedate") WebElement DueDate;
	
	@FindBy(id="components-multi-select") WebElement Components;
	
	@FindBy(id="versions-multi-select") WebElement AffectVersions;
	
	@FindBy(id="fixVersions-multi-select") WebElement FixVersions;
	
	@FindBy(id="assignee-field") WebElement Assignee;
	
	@FindBy(id="environment") WebElement Environment;
	
	@FindBy(id="description") WebElement Description;
	
	@FindBy(id="customfield_10010") WebElement RandomText;
	
	@FindBy(id="customfield_10041") WebElement VendorDeliveryDate;
	
	@FindBy(id="customfield_10040") WebElement Vendor;
	
	@FindBy(id="timetracking") WebElement OriginalEstimate;
	
	@FindBy(id="customfield_10061") WebElement AnimalsType;
	
	@FindBy(id="customfield_10061:1") WebElement Animals;
	
	@FindBy(id="customfield_11930-field") WebElement Sprint;
	
	@FindBy(id="edit-issue-submit") WebElement EditIssueBtn;
	
	@FindBy(className="cancel") WebElement CancelBtn;
	
	@FindBy(className="aui-message") WebElement DialogMessage;

	@FindBy(id="edit-issue-submit") WebElement updateIssueBtn;
	
	public Jira_EditIssuePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		this.get();
		this.mapObject();
		TestReport.report("Opened Edit Issue Dialog");
	}
	
	private void mapObject() {
		ObjMap = new HashMap<String, WebElement>();
		TabMap = new HashMap<WebElement, List<String>>();
		ObjMap.put("Project", Project);
		ObjMap.put("Bug", IssueType);
		ObjMap.put("Summary", Summary);
		ObjMap.put("SecurityLevel", SecurityLevel);
		ObjMap.put("Priority", Priority);
		ObjMap.put("DueDate", DueDate);
		ObjMap.put("Components", Components);
		ObjMap.put("AffectVersions", AffectVersions);
		ObjMap.put("FixVersions", FixVersions);
		ObjMap.put("Assignee", Assignee);
		ObjMap.put("Environment", Environment);
		ObjMap.put("Description", Description);
		ObjMap.put("RndText", RandomText);
		ObjMap.put("VendorDeliveryDate", VendorDeliveryDate);
		ObjMap.put("Vendor", Vendor);
		ObjMap.put("OriginalEstimate",OriginalEstimate);
		ObjMap.put("Sprint",Sprint);
		ObjMap.put("AnimalsType",AnimalsType);
		ObjMap.put("Animals",Animals);
		
		tab1Elem = Arrays.asList("Summary","Description","Bug","Environment","Components",
				"AffectVersions","FixVersions","SecurityLevel","OriginalEstimate","Assignee"
				,"DueDate","RndText","Vendor","VendorDeliveryDate","Priority","Sprint");
		tab2Elem = Arrays.asList("AnimalsType","Animals");
		tab3Elem = Arrays.asList("BurgerOptions");
		
		TabMap.put(tabOne,tab1Elem);
		TabMap.put(tabTwo,tab2Elem);
		TabMap.put(tabThree,tab3Elem);
		
	}
	
	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(driver.getTitle().startsWith("Edit Issue"));
	}
	
	@Override
	protected void load() {
		Assert.assertTrue(editBtn.isDisplayed());
		new Actions(driver).click(editBtn).perform();
		new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(dialog_EditIssue));
	}
	
	public void updateIssue(LinkedHashMap<String, String> dataMap) {
		for(String keyWord:dataMap.keySet()){
			filterKey(keyWord, dataMap.get(keyWord),"set");
		}
		updateIssueBtn.click();
		new WebDriverWait(driver, 20).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver drive) {
				return (!driver.getTitle().startsWith("Edit Issue"));
			}
		});
		Assert.assertFalse(driver.getTitle().startsWith("Edit Issue"),
				"Checking the title whether Edit Dialog is closed. Title:"+driver.getTitle());
	}

	public void checkIssueUpdated(LinkedHashMap<String, String> dataMap) {
		TestReport.report("Checking Issue is updated correctly");
		driver.navigate().refresh();
		new Actions(driver).click(editBtn).perform();
		String data;
		new WebDriverWait(driver, 20).until(ExpectedConditions.titleContains("Edit Issue"));
		for(String keyWord:dataMap.keySet()){
			data = filterKey(keyWord, "", "get");
			if(data.equals("BurgerOptions")){
				continue;
			}
			Assert.assertEquals(data, dataMap.get(keyWord));
			TestReport.report("Data "+ data + " is entered correctly in Element '"+keyWord+"'.");
		}
	}

	
	private String filterKey(String keyWord,String data,String operation){
		String returnData = null;
		if(operation.equals("set")){
			TestReport.report("Entering Data '"+data+"' in Element '"+keyWord+"'.");	
		}
		
		for(WebElement elem:TabMap.keySet()){
			if(TabMap.get(elem).contains(keyWord)){
				elem.click();
				break;
			}
		}
		
		if(selectElem.contains(keyWord)){
			if(operation.equalsIgnoreCase("set")){
				try {
					selectData(keyWord, data);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else returnData = getSelectedData(keyWord);
		}else if(tagElem.contains(keyWord)){
			if(operation.equalsIgnoreCase("set")) setSelectedTags(keyWord,data);
			else returnData = getSelectedTags(keyWord); 
		}else if(keyWord.equalsIgnoreCase("BurgerOptions")){
			if(operation.equalsIgnoreCase("set")) checkBurgerOptions(data);
			else returnData = "BurgerOptions"; 
		}else{
			if(operation.equalsIgnoreCase("set")){
				try {
					enterData(keyWord, data);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else returnData = getElementData(keyWord);
		}
		if(operation.equals("set")) return null;
		else return returnData;
	}

	private String getSelectedTags(String keyWord) {
		StringBuffer selectedtTags = new StringBuffer();
		List<WebElement> selectTags = ObjMap.get(keyWord).findElements(By.className("value-text"));
		for(WebElement eachTag:selectTags){
			selectedtTags.append(eachTag.getText());
			selectedtTags.append(",");
		}
		return selectedtTags.deleteCharAt(selectedtTags.length()-1).toString();
	}

	private void setSelectedTags(String keyWord, String data) {
		List<WebElement> selectTags = ObjMap.get(keyWord).findElements(By.className("item-delete"));
		for(WebElement eachElem:selectTags){
			eachElem.click();
		}
		if(data.contains(",")){
			for(String eachData:data.split(",")){
				ObjMap.get(keyWord).findElement(By.tagName("textarea")).sendKeys(eachData,Keys.ENTER);
			}
		}else{
			ObjMap.get(keyWord).findElement(By.tagName("textarea")).sendKeys(data,Keys.ENTER);
		}
		
	}

	private String getSelectedData(String keyWord) {
		return new Select(ObjMap.get(keyWord)).getFirstSelectedOption().getText();
	}

	private String getElementData(String keyWord) {
		return ObjMap.get(keyWord).getText();
	}

	private void checkBurgerOptions(String data){
		if(data.contains(",")){
			for(String eachData:data.split(",")){
				driver.findElement(By.xpath("//div[@class='checkbox']/label[.='"+eachData+"']/preceding-sibling::input[@type='checkbox']")).click();
			}
		}else{
			driver.findElement(By.xpath("//div[@class='checkbox']/label[.='"+data+"']/preceding-sibling::input[@type='checkbox']")).click();
		}
	}
	
	public void selectData(String keyWord,String data) throws InterruptedException{
		int count = 1;
		while(!ObjMap.get(keyWord).isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
		Select elem = new Select(ObjMap.get(keyWord));
		if(elem.isMultiple()) elem.deselectAll();
		elem.selectByVisibleText(data);
	}
	
	public void enterData(String keyWord,String data) throws InterruptedException{
		WebElement elem = ObjMap.get(keyWord);
		int count = 1;
		while(!elem.isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
		elem.clear();
		elem.sendKeys(data,Keys.TAB);
		count = 1;
		while(!elem.isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
	}
}
