package jiraPages;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import commonUtils.TestReport;

/**
 * Jira_CreateIssuePage has the page components of Create Issue dialog
 * opened by clicking on the Create button
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */

public class Jira_CreateIssuePage extends LoadableComponent<Jira_CreateIssuePage> {
	
	protected WebDriver driver;
	
	private HashMap<String,WebElement> ObjMap;
	
	private List<String> selectElem = Arrays.asList("SecurityLevel","Vendor","AnimalsType","Animals"); 
	private List<String> tagElem = Arrays.asList("Components","AffectVersions","FixVersions","Labels");
	
	@FindBy(id="create-issue-dialog") WebElement dialog_CreateIssue;
	
	@FindBy(id="create_link") WebElement btn_CreateIssue;
	
	@FindBy(id="project-field") WebElement Project;
	
	@FindBy(id="issuetype-field") WebElement IssueType;
	
	@FindBy(id="summary") WebElement Summary;
	
	@FindBy(id="security") WebElement SecurityLevel;
	
	@FindBy(id="priority-field") WebElement Priority;
	
	@FindBy(id="duedate") WebElement DueDate;
	
	@FindBy(id="components-multi-select") WebElement Components;
	
	@FindBy(id="versions-multi-select") WebElement AffectVersions;
	
	@FindBy(id="fixVersions-multi-select") WebElement FixVersions;
	
	@FindBy(id="assignee-field") WebElement Assignee;
	
	@FindBy(id="environment") WebElement Environment;
	
	@FindBy(id="description") WebElement Description;
	
	@FindBy(id="customfield_10010") WebElement RandomText;
	
	@FindBy(id="customfield_10041") WebElement VendorDeliveryDate;
	
	@FindBy(id="customfield_10040") WebElement Vendor;
	
	@FindBy(id="timetracking") WebElement OriginalEstimate;
	
	@FindBy(id="customfield_10061") WebElement AnimalsType;
	
	@FindBy(id="customfield_10061:1") WebElement Animals;
	
	@FindBy(id="customfield_10530") WebElement Tester;
	
	@FindBy(id="customfield_10550") WebElement VersionPicker;
	
	@FindBy(id="labels-multi-select") WebElement Labels;
	
	@FindBy(id="customfield_10653") WebElement StoryPoints;
	
	@FindBy(id="customfield_11930-field") WebElement Sprint;
	
	@FindBy(id="customfield_12931-field") WebElement EpicLink;
	
	@FindBy(id="customfield_14130") WebElement RegularExpression;
	
	@FindBy(id="qf-create-another") WebElement CreateAnotherCheckBox;
	
	@FindBy(id="create-issue-submit") WebElement CreateIssueBtn;
	
	@FindBy(className="cancel") WebElement CancelBtn;
	
	@FindBy(className="aui-message") WebElement DialogMessage;
	
	public Jira_CreateIssuePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.get();
		this.mapObject();
		TestReport.report("Loaded Create Issue Dialog");
	}

	/*As data is identified using keys, to find the respective web element, instead of using if/else if
	 * used a HashMap with the same set of keywords 
	 */
	private void mapObject() {
		ObjMap = new HashMap<String, WebElement>();
		ObjMap.put("Project", Project);
		ObjMap.put("Bug", IssueType);
		ObjMap.put("Summary", Summary);
		ObjMap.put("SecurityLevel", SecurityLevel);
		ObjMap.put("Priority", Priority);
		ObjMap.put("DueDate", DueDate);
		ObjMap.put("Components", Components);
		ObjMap.put("AffectVersions", AffectVersions);
		ObjMap.put("FixVersions", FixVersions);
		ObjMap.put("Assignee", Assignee);
		ObjMap.put("Environment", Environment);
		ObjMap.put("Description", Description);
		ObjMap.put("RndText", RandomText);
		ObjMap.put("VendorDeliveryDate", VendorDeliveryDate);
		ObjMap.put("Vendor", Vendor);
		ObjMap.put("OriginalEstimate",OriginalEstimate);
		ObjMap.put("AnimalsType",AnimalsType);
		ObjMap.put("Animals",Animals);
		ObjMap.put("Tester",Tester);
		ObjMap.put("Version",VersionPicker);
		ObjMap.put("Labels",Labels);
		ObjMap.put("StoryPoints",StoryPoints);
		ObjMap.put("Sprint",Sprint);
		ObjMap.put("Epiclink",EpicLink);
		ObjMap.put("RegularExpression",RegularExpression);
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(driver.getTitle().startsWith("Create Issue"));
	}

	@Override
	protected void load() {
		new Actions(driver).click(btn_CreateIssue).perform(); //not able to click on the webElement directly 	
		new WebDriverWait(driver, 10).until(ExpectedConditions.titleContains("Create Issue"));
	}
	
	//method to create Issue
	public String createIssue(LinkedHashMap<String,String> DataMap){
		for(String keyWord:DataMap.keySet()){
			filterKey(keyWord, DataMap.get(keyWord));
		}
		CreateIssueBtn.click();
		new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(DialogMessage));
		if(DialogMessage.isDisplayed()){
			String TaskID = DialogMessage.getText().split(" ")[1];
			TestReport.report("Task ID for the created Issue is "+ TaskID);
			return TaskID;
		}else{
			return null;
		}
	}
	
	private void filterKey(String keyWord,String data){
		TestReport.report("Entering Data '"+data+"' in Element '"+keyWord+"'.");
		if(selectElem.contains(keyWord)){
			try {
				selectData(keyWord, data);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}else if(tagElem.contains(keyWord)){
			setSelectedTags(keyWord,data);
		}else if(keyWord.equalsIgnoreCase("BurgerOptions")){
			checkBurgerOptions(data);
		}else{
			try {
				enterData(keyWord, data);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void setSelectedTags(String keyWord, String data) {
		if(data.contains(",")){
			for(String eachData:data.split(",")){
				ObjMap.get(keyWord).findElement(By.tagName("textarea")).sendKeys(eachData,Keys.ENTER);
			}
		}else{
			ObjMap.get(keyWord).findElement(By.tagName("textarea")).sendKeys(data,Keys.ENTER);
		}
	}
	
	private void checkBurgerOptions(String data){
		if(data.contains(",")){
			for(String eachData:data.split(",")){
				driver.findElement(By.xpath("//div[@class='checkbox/label[.='"+eachData+"']")).click();
			}
		}else{
			driver.findElement(By.xpath("//div[@class='checkbox/label[.='"+data+"']")).click();
		}
	}
	
	public void selectData(String keyWord,String data) throws InterruptedException{
		int count = 1;
		while(!ObjMap.get(keyWord).isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
		Select elem = new Select(ObjMap.get(keyWord));
		if(elem.isMultiple()) elem.deselectAll();
		elem.selectByVisibleText(data);
	}
	
	public void enterData(String keyWord,String data) throws InterruptedException{
		WebElement elem = ObjMap.get(keyWord);
		int count = 1;
		while(!elem.isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
		elem.clear();
		elem.sendKeys(data,Keys.TAB);
		count = 1;
		while(!elem.isEnabled()){
			if(count>5){
				break;
			}
			TimeUnit.SECONDS.sleep(2);
			count++;
		}
	}

}
