package jiraPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import commonUtils.TestReport;

/**
 * Jira_TestProject has the page components of home page opened on 
 * opening the url - https://jira.atlassian.com/browse/TST.
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */
public class Jira_TestProject extends LoadableComponent<Jira_TestProject> {
	
	protected WebDriver driver;
	private String testUrl = "https://jira.atlassian.com/browse/TST";
	private String pageTitle = "A Test Project - Atlassian JIRA";
	
	//@FindBy(xpath="//li[@id='user-options']/a[text()='Log In']")
	@FindBy(className="login-link")
	WebElement btn_Login;
	
	@FindBy(id="create_link") WebElement btn_CreateIssue;
	
	public Jira_TestProject(WebDriver drive) {
		this.driver = drive;
		PageFactory.initElements(driver, this);
		this.get();
		TestReport.report("Opened Jira Home Page");
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(driver.getTitle().equals(pageTitle));
	}

	@Override
	protected void load() {
		driver.get(testUrl);
	}
	
	public void login_Jira(String userName,String password){
		btn_Login.click();
		new Jira_LoginPage(driver).Jira_Login(userName,password);
		new WebDriverWait(driver, 20).until(ExpectedConditions.titleContains(pageTitle));
		this.isLoaded();
		TestReport.report("Logged in");
	}
	
	public Jira_CreateIssuePage clickCreateIssue(){
		return new Jira_CreateIssuePage(driver);
	}

	public Jira_IssuePage searchIssue(String issueText) {
		return new Jira_IssuePage(this.driver,issueText);
	}

}
