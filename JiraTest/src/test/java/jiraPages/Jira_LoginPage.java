package jiraPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

/**
 * Jira_LoginPage has the page components of Login page
 * opened by clicking on the login button.
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */
public class Jira_LoginPage extends LoadableComponent<Jira_LoginPage> {
	
	protected WebDriver driver;
	private String loginUrl = "https://id.atlassian.com/login/login";
	private String pageTitle = "Log in";
	
	@FindBy(id="username") WebElement edit_UserName;
	
	@FindBy(id="password") WebElement edit_Password;
	
	@FindBy(id="login-submit") WebElement btn_Login;
	
	public Jira_LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.get();
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(driver.getTitle().equals(pageTitle));
	}


	@Override
	protected void load() {
		if(driver.getTitle().equals(pageTitle)) return;
		else driver.get(loginUrl);
	}
	
	public void Jira_Login(String userName,String password){
		edit_UserName.sendKeys(userName);
		edit_Password.sendKeys(password);
		btn_Login.click();
	}

}
