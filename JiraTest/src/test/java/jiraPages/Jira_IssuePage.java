package jiraPages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import commonUtils.TestReport;

/**
 * Jira_IssuePage has the page components of Issue Page
 * opened by searching the right Issue ID.
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */
public class Jira_IssuePage extends LoadableComponent<Jira_IssuePage> {

	String searchedIssueID;
	
	private WebDriver driver;
	
	@FindBy(id="quickSearchInput") WebElement quickSearch;
	
	public Jira_IssuePage(WebDriver driver,String issueText) {
		searchedIssueID = issueText;
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.get();
		
	}
	
	public String getTitle(){
		return driver.getTitle();
	}
	
	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(this.getTitle().contains(searchedIssueID),"Searching Issue ID - "+ searchedIssueID);
		TestReport.report("Issue - "+ searchedIssueID + " found");
	}

	@Override
	protected void load() {
		TestReport.report("Searching Issue - "+ searchedIssueID);
		quickSearch.sendKeys(searchedIssueID + Keys.ENTER);
	}

	public Jira_EditIssuePage clickEditIssue() {
		return new Jira_EditIssuePage(driver);
	}
	

}
