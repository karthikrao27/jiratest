package jiraUser;

import java.util.LinkedHashMap;

import jiraPages.Jira_TestProject;

import org.openqa.selenium.WebDriver;

public class EndUser {
	
	WebDriver driver;
	
	Jira_TestProject jiraPage;
	
	public EndUser(WebDriver driver) {
		this.driver = driver;
	}
	
	public void openJira(){
		
		jiraPage = new Jira_TestProject(driver);
	}
	
	public void loginJira(String userName,String password){
		jiraPage.login_Jira(userName, password);
	}
	
	public String createIssue(LinkedHashMap<String,String> DataMap){
		return jiraPage.clickCreateIssue().createIssue(DataMap);
	}

	public String searchJiraforIssue(String issueText) {
		String result = jiraPage.searchIssue(issueText).getTitle();
		if(result.contains(issueText)) return result;
		else return null;	
	}

	public void updateIssue(LinkedHashMap<String, String> dataMap,String searchedIssue) {
		jiraPage.searchIssue(searchedIssue).clickEditIssue().updateIssue(dataMap);
	}
	
	public void checkIssueUpdated(LinkedHashMap<String, String> dataMap,String searchedIssue){
		jiraPage.searchIssue(searchedIssue).clickEditIssue().updateIssue(dataMap);
	}
}
