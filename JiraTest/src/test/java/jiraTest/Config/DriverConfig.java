package jiraTest.Config;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * DrverConfig has the methods that deals with the driver configurations
 * 
 * @author karthik.rao1
 * @since 2014-10-27
 */
public class DriverConfig {
	
	public WebDriver startDriver(String browser) throws MalformedURLException{
		
		if(SuiteConfig.gridUrl.length()!=0){
			System.out.println("Grid URL found : "+ SuiteConfig.gridUrl);
			if(browser.equalsIgnoreCase("ie")){
				return new RemoteWebDriver(
						new URL(SuiteConfig.gridUrl),
						DesiredCapabilities.internetExplorer()
						);
			}else if(browser.equalsIgnoreCase("firefox")) {
				return new RemoteWebDriver(
						new URL(SuiteConfig.gridUrl),
						DesiredCapabilities.firefox()
						);
			}else if(browser.equalsIgnoreCase("chrome")) {
				return new RemoteWebDriver(
						new URL(SuiteConfig.gridUrl),
						DesiredCapabilities.chrome()
						);
			}
		}else{
			if(browser.equalsIgnoreCase("ie")){
				if(SuiteConfig.IEDriver.length()!=0){
					System.setProperty("webdriver.ie.driver",SuiteConfig.IEDriver);
					System.out.println("Opening IE");
					return new InternetExplorerDriver();	
				}else{
					System.out.println("IE Driver not found");
					return null;
				}
			}else if(browser.equalsIgnoreCase("firefox")) {
				return new FirefoxDriver();
			}else if(browser.equalsIgnoreCase("chrome")) {
				if(SuiteConfig.ChromeDriver.length()!=0){
					System.setProperty("webdriver.chrome.driver",SuiteConfig.ChromeDriver);
					System.out.println("Opening Chrome");
					return new ChromeDriver();	
				}else{
					System.out.println("Chrome Driver not found");
					return null;
				}
			}
		}
		
		return new FirefoxDriver();
	}
}
