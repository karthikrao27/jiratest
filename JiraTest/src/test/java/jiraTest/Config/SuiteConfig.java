package jiraTest.Config;

import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;

import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import commonUtils.TestReport;


public class SuiteConfig
{
	public static String gridUrl;
	public static String userName;
	public static String userPwd;
	public static Map<String,String> testNgParam;
	public static String IEDriver;
	public static String ChromeDriver;
	
	@BeforeSuite
	public void initSuite(ITestContext context){
		
		TestReport.report("--------------------------------------------------------------");
		TestReport.report("Starting Suite:"+ context.getSuite().getName());
		TestReport.report("--------------------------------------------------------------");
		
		Properties config = new Properties();
		try {
			config.load(new FileInputStream("src/test/resource.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		gridUrl = config.getProperty("gridUrl");
		userName = config.getProperty("loginUserId");
		userPwd = config.getProperty("loginUserPwd");
		IEDriver = config.getProperty("IEDriver");
		ChromeDriver = config.getProperty("ChromeDriver");
		
	}
	
	@AfterSuite
	public void cleanSuite(){
		TestReport.report("Ending Suite");
		TestReport.report("--------------------------------------------------------------");
	}
  
}
