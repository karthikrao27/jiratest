package jiraTest.Config;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commonUtils.LinkedProperties;
import commonUtils.TestReport;

/**
 * TestConfig class contains all the beforetest,aftertest,beforemethod,aftermethod
 * 
 * 
 * @author karthik.rao1
 *
 */
public class TestConfig extends SuiteConfig{
	
	private WebDriver driverObj;

	public WebDriver getDriver(){
		return driverObj;
	}
	
	@Parameters({"browser"})
	@BeforeMethod
	public void testSetup(String browser,Method testMethod) throws MalformedURLException{
		TestReport.report("--------------------------------------------------------------");
		TestReport.report("Starting Test:"+ testMethod.getAnnotation(Test.class).testName());
		TestReport.report("Test Description: "+ testMethod.getAnnotation(Test.class).description());
		TestReport.report("--------------------------------------------------------------");
		this.driverObj =new DriverConfig().startDriver(browser);
		Assert.assertNotNull(driverObj,"Checking driver obj is created successfully");
		this.driverObj.manage().window().maximize();
	}
	
	@AfterMethod
	public void testCleanUp(){
		TestReport.report("--------------------------------------------------------------");
		this.driverObj.quit();
	}

	public static LinkedHashMap<String, String> gatherData(String dataMapfile) {
		LinkedHashMap<String, String> dataMap = new LinkedHashMap<String, String>();
		LinkedProperties dataFile = new LinkedProperties();
		try {
			dataFile.load(new FileInputStream(dataMapfile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		TestReport.report("Test Data Used ");
		TestReport.report("*********************************");

		Enumeration<?> propertySet = dataFile.propertyNames();
		while(propertySet.hasMoreElements()){
			String key = (String) propertySet.nextElement();
			TestReport.report("Key, data :"+ key +", "+ dataFile.getProperty(key));
			dataMap.put(key, dataFile.getProperty(key));
		}
		TestReport.report("*********************************");
		return dataMap;
	}


}
