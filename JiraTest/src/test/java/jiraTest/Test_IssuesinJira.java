package jiraTest;

import java.net.MalformedURLException;
import java.util.LinkedHashMap;

import jiraTest.Config.SuiteConfig;
import jiraTest.Config.TestConfig;
import jiraUser.EndUser;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Test_IssuesinJira extends TestConfig {
	
	@Test(testName="Create Issue",description="Creates Issue in project TST from the data given in DataFile parameter")
	@Parameters({"DataFileforCreateIssue"})
	public void test_CreateIssue(String DataMapfile) throws MalformedURLException{
		LinkedHashMap<String, String> dataMap = TestConfig.gatherData(DataMapfile);
		EndUser endUser = new EndUser(getDriver());
		endUser.openJira();
		endUser.loginJira(SuiteConfig.userName, SuiteConfig.userPwd);
		String status = endUser.createIssue(dataMap);
		Assert.assertNotNull(status, "Issue Creation");
	}

	@Test(testName="Search Issue without Logging in",description="Searching issue without logging in")
	@Parameters({"IssuetobeSearched"})
	public void test_SearchIssuewithoutLoggingIn(String issueText) throws MalformedURLException{
		EndUser endUser = new EndUser(getDriver());
		endUser.openJira();
		String status = endUser.searchJiraforIssue(issueText);
		Assert.assertNotNull(status, "Issue Search");
	}
	
	@Test(testName="Search Issue with logging in",description="Searching issue after logging in")
	@Parameters({"IssuetobeSearched"})
	public void test_SearchIssueLoggedIn(String issueText) throws MalformedURLException{
		EndUser endUser = new EndUser(getDriver());
		endUser.openJira();
		endUser.loginJira(SuiteConfig.userName, SuiteConfig.userPwd);
		String status = endUser.searchJiraforIssue(issueText);
		Assert.assertNotNull(status, "Issue Search");
	}
	
	@Test(testName="Update Issue",description="Update an existing Issue")
	@Parameters({"DataFileforUpdateIssue","updateIssueId"})
	public void test_UpdateIssue(String DataMapfile,String serachedIssueId) throws MalformedURLException{
		LinkedHashMap<String, String> dataMap = TestConfig.gatherData(DataMapfile);
		EndUser endUser = new EndUser(getDriver());
		endUser.openJira();
		endUser.loginJira(SuiteConfig.userName, SuiteConfig.userPwd);
		endUser.updateIssue(dataMap, serachedIssueId);
		endUser.checkIssueUpdated(dataMap, serachedIssueId);
	}
	
}
