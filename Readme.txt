How to run:

You can use either ide to run the testng file directly or you can run through the maven command.

The browser specification (firefox/chrome) should be specified in the testng.xml file.

The other parameters required for creating issue and updating issue are passed from the properties file stored
in the file path "\src\test\data". You can go refer the dataTemplate for the keywords to use for creating the data.

In the file "src\resource.properties", the user name and password should be given for the login to happen

If you like to run the tests using a remote driver you can give the grid url in the resource.properties file
The IEdriver and chromedriver path needs to be given in the resource.properties file

For Test conditions where

1. new Issues can be created
	You can update the 'dataforCreateIssue.properties' file to pass your preferred data.
	For reference you can refer the dataTemplate for the keywords

2. Existing Issue can be updated
	You can update the 'dataforEditIssue.properties' file to pass your preferred data.
	For reference you can refer the dataTemplate for the keywords

3. Existing Issue can be searched
	You can pass the test ID in the parameter 'IssuetobeSearched'

Assumptions:

1. Only the Test ID is used to search the Issue and its searched from the Search box present in the top right corner
2. Editing issue is done by clicking on the edit button rather than directly updating it from the issue page